--1. We’ve noticed that there’s something wrong with the event logging: it sometimes logs a wrong user_id which does not even exist in our database. We suspect that the problem is only related to some event types. List the event types where the problem occurs the most often.

select       event_type
            ,count(1) as cnt
from        user_interaction ui
where       ui.user_id not in (select user_id from users) 
group by    event_type
order by    count(1) desc

--2. We have spotted that we’ve had a lot of events duplicated on a specific date. What date was that?

with         base as 
(
select       event_id
            ,event_time
            ,count(1) as cnt
from        user_interaction ui
group by    event_id
            ,event_time
)
select distinct 
             cast(event_time as date) as date
from        base
where       cnt > 1

--3. What country do most of our website visitors come from? 🌐
with       base as
(
select     country
          ,count(1) as cnt
from       users
group by   country
),
           maxvisiter as
(           
select     max(cnt) as cnt
from       base
)
select     country
from       base
inner join  maxvisiter
on         maxvisiter.cnt = base.cnt

--4.Our most valuable users are the ones who have completed a minimum of 5 VIEWs and 10 CLICKs. What age group do they fall in: 18-49 or 50-80?

with     ui as
(
select distinct * from user_interaction
-- the user_interaction table is duplicated (mentioned earlier). Need to remove duplicates
),
          activitycount as 
(
select      ui.user_id
           ,cast(left(age,2) as int) as starofagerange
           ,cast(right(age,2) as int) as endofagerange
           ,sum(case when event_type = 'VIEW' then 1 else 0 end) as VIEW
           ,sum(case when event_type = 'CLICK' then 1 else 0 end) as CLICK
from        ui
left join   users
on          ui.user_id = users.user_id
group by    ui.user_id,cast(left(age,2) as int),cast(right(age,2) as int)
)
select      case when starofagerange >= 18 and endofagerange <= 49 then '18-49'
                 when starofagerange >= 50 and endofagerange <= 80 then '50-80'
                 else null end age_group
from        activitycount
where       VIEW >= 5 
            and CLICK >= 10

--5. What do you think of the query below? Share your thoughts in a few sentences. 💡
--I assume that the author wants to create a table that takes the invoice information with account_number = 1234 from two tables(only take invoice_number has in both of tables). However I recognize some bugs from the code. I'd like to adjust like the code bellow:

create table account_invoice_records
as
select
          ih.invoice_number
        , ih.account_number
        , ih.invoice_created_date
        , ih.invoice_due_date
        , ii.product_number
        , ii.net_amount
        , ii.tax_amount
from    invoice_headers ih   -- 10m+ records
join    invoice_items ii   -- 100m+ records
on      ih.invoice_number = ii.invoice_number
where   ih.account_number = 1234
;

--6.Create a matrix with these parameters:
--Rows: user country
--Columns: user age
--Measure: count of interactions
with        base as
(
select      u.country
            ,u.age
            ,count(distinct event_id) as interactons_cnt
from        users u
left join   user_interaction ui
on          u.user_id = ui.user_id
group by    u.country,u.age
)
select      country
            ,sum( case when age = '18-45' then interactons_cnt else 0 end) as "18-45"
            ,sum( case when age = '46-80' then interactons_cnt else 0 end) as "46-80"
from base
group by    country

--7. Create a table which shows the user interactions and the previous interaction event for every record. Display the previous event_type and event_time.
create table user_interactions as
select 
             event_type as current_interaction
            ,event_time
            ,lag(event_type) over (order by event_time) as previous_interaction
from        user_interaction
order by    event_time desc

--8. We need to identify the first and the last interaction for every day. Show the whole record for the first and last interactions for each day.
with      base as 
(
select 
          cast(event_time as date) as date_
          ,event_time
          ,event_type
from      user_interaction
),
          first_interaction as 
(
select    date_
          ,event_type
          ,event_time,rank() over(partition by date_ order by event_time) as rank_
from      base
group by  date_
          ,event_type
          ,event_time
), 
          last_interaction as
(
select    date_
          ,event_type
          ,event_time
          ,rank() over(partition by date_ order by event_time desc) as rank_
from      base
group by  date_
          ,event_type
          ,event_time
)
 select   date_
          ,event_type
          ,'fist_interaction' as type
from      first_interaction
where     rank_ = 1
union all 
select    date_
          ,event_type
          ,'last_interaction' as type
from      last_interaction
where     rank_ = 1
 
--9. Describe the result of this query. What is it used for? 🤔
The script is to take the list of email and IP country from who:
- plan date is on today and plan type is free
- using gmail
- having the last IP address in UK or N/A 
- they only use the application on the day they resigst the account.
